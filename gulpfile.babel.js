import gulp from 'gulp'
import _ from 'lodash'
import mjml from 'gulp-mjml'
import del from 'del'
import browsersync from 'browser-sync'

const browserSync = browsersync.create()

var options = () => {
	let config = {
		defaults: {
			configPath: './aca.config.js',
			paths: {
				build: './build',
				templates: './templates',
				archive: './archive',
				html_archive: './html_archive',
				content: './content'
			},
			watch: [ './templates/*.mjml', './partials/*.mjml', './content/**/*.mjml' ],
			browsersync: {
				baseDir: './build',
				port: 3000,
				directory: true,
				browser: 'google chrome'
			},
			mjml: {},
			mailer: {
				host: 'secure.emailsrvr.com',
				port: 587,
				secure: false,
				account: {
					user: 'jonathan@acadianacenterforthearts.org',
					pass: 'AcA#7060'
				},
				message: {
					sender: '"Jonathan Armentor" <jonathan@acadianacenterforthearts.org>',
					rec: 'Jonathan A. <jarmentor@me.com>',
					subject: '[TEST] E-Blast Test',
					html: '<h1>Content Empty</h1>'
				}
			}
		}
	}

	try {
		config.user = require(config.defaults.configPath)
	} catch (e) {
		config.user = {}
		console.log(`No user config found at ${config.defaults.configPath}. Running task using default options.`)
	}
	return _.merge(config.defaults, config.user)
}
options = options()

const cleanUp = (dir) => {
	del.sync(options.paths.build + '/*')
}

const listTemplates = (opts) => {
	opts = _.merge({ extensions: false }, opts || {})
	let files
	try {
		let fs = require('fs')
		files = fs.readdirSync(options.paths.templates)
	} catch (e) {
		files = []
		console.log('unable to load FS module. returning empty array')
	}
	if (!opts.extensions) {
		files = files.map((file) => file.replace('.mjml', ''))
	}
	return files
}

// const mailer = () => {
// 	let content = '<h1>Blue</h1>'
// 	nodemailer.createTestAccount((err, account) => {
// 		let transporter = nodemailer.createTransport({
// 			host: options.mailer.host,
// 			port: options.mailer.port,
// 			secure: options.mailer.secure,
// 			auth: {
// 				user: options.mailer.account.user,
// 				pass: options.mailer.account.pass
// 			}
// 		})

// 		let mailOptions = {
// 			from: options.mailer.message.sender,
// 			to: options.mailer.message.rec,
// 			subject: '[TEST] E-Blast Test',
// 			html: content
// 		}

// 		transporter.sendMail(mailOptions, (error, info) => {
// 			if (error) {
// 				return console.log(error)
// 			}
// 			console.log('Message sent: %s', info.messageId)
// 			console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info))
// 		})
// 	})
// }

gulp.task('templates', () => console.log(listTemplates({ extensions: true })))
gulp.task('config', () => console.log(options))

gulp.task('mail', () => mailer())

gulp.task('clean_html_archive', () => {
	cleanUp(options.paths.html_archive)
})

gulp.task('clean', () => {
	cleanUp(options.paths.build)
})

gulp.task('reload', [ 'build' ], () => browserSync.reload())

gulp.task('start', [ 'build' ], () => {
	browserSync.init({ server: options.browsersync })
	gulp.watch(options.watch, [ 'reload' ])
})

gulp.task('archive', [ 'clean_html_archive' ], () =>
	gulp
		.src(options.paths.archive + '/*.mjml')
		.pipe(mjml())
		.pipe(gulp.dest(options.paths.html_archive))
		.pipe(browserSync.stream())
)

gulp.task('build', [ 'clean' ], () =>
	gulp
		.src(options.paths.templates + '/*.mjml')
		.pipe(mjml())
		.pipe(gulp.dest(options.paths.build))
		.pipe(browserSync.stream())
)

gulp.task('default', [ 'build' ])

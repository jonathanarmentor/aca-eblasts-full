const config = {
	browsersync: {
		port: 4000
	},
	mailer: {
		imap_server: '',
		username: '',
		password: ''
	}
}

module.exports = config
